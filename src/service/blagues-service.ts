import type { blagues, categories } from '@/entities'
import axios from 'axios'

export async function fetchBlague() {
  const response = await axios.get<blagues[]>('http://localhost:8080/api/blagues')
  return response.data
}

export async function fetchBlaguesByCategorieId(id_categorie: number) {
  const response = await axios.get<blagues[]>('http://localhost:8080/api/blagues/categorie/' + id_categorie);
  return response.data;
}

export async function postBlague(blagues: blagues) {
  const response = await axios.post<blagues>('http://localhost:8080/api/blagues/', blagues);
  return response.data;
}

export async function deleteBlague(id: number) {
  await axios.delete<void>('http://localhost:8080/api/blagues/' + id);
}