import type { categories } from '@/entities'
import axios from 'axios'

export async function fetchCategorie() {
  const response = await axios.get<categories[]>('http://localhost:8080/api/categories')
  return response.data
}

export async function fetchOneCategorie(id:number) {
  const response = await axios.get<categories>('http://localhost:8080/api/blagues/'+id);
  return response.data;
}

export async function deleteCategorie(id:any) {
  await axios.delete<void>('http://localhost:8080/api/categories/'+id);
}