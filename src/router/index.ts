import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '@/views/HomeView.vue';
import BlaguesVue from '@/views/BlaguesVue.vue';
import LoginView from '@/views/LoginView.vue';
import AddBlagueView from '@/views/AddBlagueView.vue';
import CreateUser from '@/views/CreateUser.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: '/', name: 'home', component: HomeView },
    { path: '/blague', name: 'liste-blagues', component: BlaguesVue },
    { path: '/categorie/:idCategorie/blagues', name: 'liste-blague-par-categorie', component: BlaguesVue },
    { path: '/login', name: 'login', component: LoginView },
    { path: '/add-blague', name: 'add-blague', component: AddBlagueView },
    { path: '/signup', name: 'signup', component: CreateUser },
  ]
})

export default router
