export interface blagues {
  id?: number,
  content: string,
  id_categories: number,
  categories?: categories
}

export interface categories {
  id: number
  name: string
  description: string
}

export interface User {
  id?: number;
  email: string;
  password?: string;
  role?: string;
}
